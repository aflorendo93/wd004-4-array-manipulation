const pila = [

];


function addQueue(newQueueEnd){
	pila.push(newQueueEnd);
	return "Bagong pumila ng tama: " + newQueueEnd;
}

function showAll(){
	return pila;
}

function deQueue(){
	pila.shift();
	return "Natanggal na ang una sa pila.";
}

function expedite(index){
	pila.splice(index, 1);
	return "Natanggal na ang hiniling sa pila.";
}

function reverseDequeue(){
	pila.pop();
	return "Natanggal na ang huli sa pila.";
}

function addVIP(newQueueStart){
	pila.unshift(newQueueStart);
	return "Bagong pumila pa-espesyal: " + newQueueStart;
}

function updateName(index, newName){
	pila.splice(index, 1, newName);
	return "Nabago na ang pangalan sa: " + newName;
}