//Array --- is a collection og related data. Denoted by [].
//Ex:

const fruits = ['Apple', 'Banana', 'Kiwi'];

const ages = [1,2,3,4];

const students = [ 
	{name: "brandon", age: 11}, 
	{name: "Brenda", age: 12},
	{name: "celmer", age: 15},
	{name: "archie", age: 16},
];

//it is recommended to use plural form when naming an array, and singular when namin an object

// to access an array, we need to use bracket notation and the index of the item we want to access.
//
//index is the position of the data relative to the position of the first data.
// index always starts at 0;
// max index is number of items in index -1
//
//To add a data in an array,
//we will use the method .push();
//array.push(dataWeWantToAdd);
//the method will return the length of the array
//length means # of data inside an array
//The new data will be added at the end.
//
//array[index] = newValue;
//to update a data, access the data first via bracket notation and then re-assign the value.
// to delete a data at the end of an array, we use the method pop();
//to delete a data at the start of an array, we use the method shift();
//array.shift();
//It will return the deleted data;
//To add a data in the beginning of an array, we use the emthod unshift(dataWeWantToAdd);
//array.unshift(dataWeWantToAdd);
//This will return the new length of the array
//
// push--- add / end
// pop --- delete / end
// shift --- delete / start
// unshift --- add / start
//
//.splice();
// It takes at least 2 arguments.
//array.splice(startingIndex,noOfItemsYouWantToDelete, --optional dataWeWantToInsert );
//This will return an array of deleted items;



