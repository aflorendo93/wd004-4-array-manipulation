// 1. Arithmetic
// 2. Assignment
// 3. Arithmetic-Assignment
// 4. Comparison Operators
// 		A. Equality Operator (==)
//		It compares the values REGARDLESS of datatype.
//		Ex:
//		2 == 2 // true
//		2 == 3 // false
//     '2' == 2 // true (String vs Number)
//		1 == // false
//      0 == false // true -- truthy
//		1 == false // false -- falsy
//		1 == true // true -- truthy
// 		the rest == false //
//
//
//		B. Strict Equality Operator (====) : It Compares the values and the data types 