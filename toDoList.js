const todos = [
	
];


function addTodo(task){
	const newTodo = task;
	todos.push(newTodo);
	return "Successfully added " + newTodo;
}

function showTodos(){
	return todos;
}

function updateTodo(index, updatedTask){
	todos[index] = updatedTask;
	return "Succesfully updated task!";
}

function deleteTodo(index) {
	todos.splice(index, 1);
	return "Successfully deleted task!"; 
}

